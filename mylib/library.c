#include "library.h"

#include <stdio.h>

void hello(void) {
  printf("Hello, World!!\n");
#ifdef EXTRA
  printf("extra!\n");
#endif
}
