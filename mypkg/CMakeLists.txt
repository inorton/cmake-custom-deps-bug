add_custom_target(libs
  ALL
  DEPENDS mylib)

add_custom_command(
  OUTPUT ${CMAKE_BINARY_DIR}/mypkg.stamp
  DEPENDS libs
  COMMAND date >> ${CMAKE_SOURCE_DIR}/custom_command_executed.log
  COMMAND date > ${CMAKE_SOURCE_DIR}/mylib/metadata.txt
  COMMAND tar -C ${CMAKE_SOURCE_DIR} -cvf ${CMAKE_BINARY_DIR}/mypkg-src.tar mylib
  COMMAND ${CMAKE_COMMAND} -E touch ${CMAKE_BINARY_DIR}/mypkg.stamp
)

add_custom_target(mypkg
  ALL
  DEPENDS ${CMAKE_BINARY_DIR}/mypkg.stamp
  )
