#!/bin/sh

set -e
rm -f mylib/metadata.txt
rm -f *.log
rm -rf b
mkdir b
cd b

echo create 'extra.h'
echo '#define EXTRA 1' > ../mylib/extra.h

CC=gcc cmake ..
echo doing first build
cmake --build .
echo checking library
ls -l mylib/libmylib.a

echo checking tar exists
ls -l mypkg-src.tar
echo force rebuild of mylib by editing extra.h
echo "" > ../mylib/extra.h

sleep 2

# alter the metadata, the custom command should re-generate this file
echo REBUILD > ../mylib/metadata.txt
cmake --build .
echo mylib should have been rebuilt too, check
ls -l mylib/libmylib.a

echo tar and metadata should have been regenerated and not contain REBUILD
tar -xf mypkg-src.tar -O mylib/metadata.txt > metadata.txt
echo metadata content is...
cat metadata.txt

echo mylib/metadata.txt should contain a date now - should not contain REBUILD
cat ../mylib/metadata.txt |grep REBUILD && exit 1
