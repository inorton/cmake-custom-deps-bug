cmake_minimum_required(VERSION 3.10)
project(cmakebug C)

set(CMAKE_C_STANDARD 99)

add_subdirectory(mylib)
add_subdirectory(mypkg)
